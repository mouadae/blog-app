package me.ktkim.blog;

import me.ktkim.blog.common.util.AuthProvider;
import me.ktkim.blog.model.domain.Ftable;
import me.ktkim.blog.model.domain.User;
import me.ktkim.blog.repository.TestUserRepository;
import me.ktkim.blog.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;

/**
 * @author Kim Keumtae
 */
@SpringBootApplication
public class BlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogApplication.class, args);
    }

    @Bean
    CommandLineRunner start(TestUserRepository userRepository , UserRepository us){
        return args -> {
//            userRepository.save(new Ftable(1,"hamza"));

        };
    }
}
